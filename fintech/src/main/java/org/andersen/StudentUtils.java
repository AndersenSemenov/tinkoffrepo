package org.andersen;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class StudentUtils {
    public static int getSumAge(List<Student> listOfStudents, String name) {
        return listOfStudents.stream()
                .filter(student -> student.getName().equals(name))
                .reduce(0, (acc, student) -> acc + student.getAge(), Integer::sum);
    }

    public static Set<String> getStudentsNames(List<Student> listOfStudents) {
        return listOfStudents.stream()
                .map(student -> student.getName())
                .collect(Collectors.toSet());
    }

    public static boolean consistsStudentWithAge(List<Student> listOfStudents, int age) {
        return listOfStudents.stream().anyMatch(student -> student.getAge() > age);
    }

    public static Map<UUID, String> getMapUidName(List<Student> listOfStudents) {
        return listOfStudents.stream().collect(Collectors.toMap(k -> k.getUid(), v -> v.getName()));
    }

    public static Map<Integer, List<Student>> getMapOfStudentsGroupedByAge(List<Student> listOfStudents) {
        return listOfStudents.stream().collect(Collectors.groupingBy(Student::getAge));
    }
}
