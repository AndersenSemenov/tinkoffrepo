package org.andersen;

import lombok.*;
import java.util.UUID;

@Getter
@Setter
public class Student {
    private final UUID uid;
    private int age;
    private String name;

    public Student(int age, String name) {
        this.uid = UUID.randomUUID();
        this.age = age;
        this.name = name;
    }

    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
        } else {
            throw new IllegalArgumentException("Age can't be non positive");
        }
    }
}
