package org.andersen;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    Student studentGri = new Student(19, "Gri");
    Student studentSergey = new Student(15, "Sergey");
    Student studentAlex = new Student(19, "Alex");
    List<Student> lst = List.of(new Student(10, "Andrey"),new Student(12, "Andrey"),
            new Student(10, "Nick"),new Student(5, "George"),
            new Student(29, "Nick"),new Student(24, "Ron"));

    @Test
    void setAgeShouldBePositive() {
        assertThrows(IllegalArgumentException.class, () -> studentSergey.setAge(-2));
    }

    @Test
    void sumAgeOfAndreyShouldBeTwentyTwo() {
        assertEquals(22, StudentUtils.getSumAge(lst, "Andrey"));
    }

    @Test
    void sumAgeOfWerShouldBeTwentyTwo() {
        assertEquals(0, StudentUtils.getSumAge(lst, "Wer"));
    }

    @Test
    void sumAgeWithNullListShouldNotBeNull() {
        assertThrows(NullPointerException.class, () -> StudentUtils.getSumAge(null, "Wer"));
    }

    @Test
    void getStudentsNamesTestCase() {
        var expectedSet = Set.of("Andrey", "Nick", "George", "Ron");
        assertEquals(expectedSet, StudentUtils.getStudentsNames(lst));
    }

    @Test
    void lstShouldContainStudentOlderThanEighteen() {
        assertEquals(true, StudentUtils.consistsStudentWithAge(lst, 18));
    }

    @Test
    void lstShouldNotContainStudentOlderThanFifty() {
        assertNotEquals(true, StudentUtils.consistsStudentWithAge(lst, 50));
    }

    @Test
    void getMapUidNameTestCase() {
        var expectedMap = Map.of(studentGri.getUid(), studentGri.getName(),
                                                studentSergey.getUid(), studentSergey.getName(),
                                                studentAlex.getUid(), studentAlex.getName());

        assertEquals(expectedMap, StudentUtils.getMapUidName(List.of(studentGri, studentSergey, studentAlex)));
    }

    @Test
    void getMapOfStudentsGroupedByAgeTestCase() {
        var expectedMap = Map.of(19, List.of(studentGri, studentAlex),
                                                    15, List.of(studentSergey));

        assertEquals(expectedMap, StudentUtils.getMapOfStudentsGroupedByAge(List.of(studentGri, studentSergey, studentAlex)));
    }
}