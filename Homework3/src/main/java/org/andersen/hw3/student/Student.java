package org.andersen.hw3.student;

import com.opencsv.bean.CsvBindByName;
import lombok.*;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class Student {
    private final UUID uid = UUID.randomUUID();

    @CsvBindByName
    private String name;

    @CsvBindByName
    private int age;

    @CsvBindByName(column = "time_from")
    private int timeFrom;

    @CsvBindByName(column = "time_to")
    private int timeTo;

    public Student() {}

    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
        } else {
            throw new IllegalArgumentException("Age can't be non positive");
        }
    }
}
