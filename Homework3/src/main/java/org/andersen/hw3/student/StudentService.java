package org.andersen.hw3.student;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.andersen.hw3.annotation.CountStudents;

import java.io.FileNotFoundException;
import java.time.LocalTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    @CountStudents
    public List<Student> getBusyStudents() throws FileNotFoundException {
        studentRepository.parseFile();

        int time = LocalTime.now().getHour();

        return studentRepository.listOfStudents
                .stream()
                .filter(student -> isBusy(student, time))
                .toList();
    }

    private boolean isBusy(Student student, int time) {
        int start = student.getTimeFrom();
        int end = student.getTimeTo();
        if (start < end) {
            return start <= time && time <= end;
        } else {
            return start < time || time <= end;
        }
    }
}