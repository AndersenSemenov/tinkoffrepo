package org.andersen.hw3.student;

import lombok.RequiredArgsConstructor;
import org.andersen.hw3.ConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;


@Component
@RequiredArgsConstructor
public class StudentScheduler {
    private final StudentService studentService;

    @Scheduled(cron = "${spring.boot.config.cron}")
    public void executeStudentServiceMethod() throws FileNotFoundException {
        studentService.getBusyStudents();
   }
}
