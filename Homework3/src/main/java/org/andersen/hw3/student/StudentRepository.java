package org.andersen.hw3.student;

import com.opencsv.bean.CsvToBeanBuilder;
import lombok.*;
import org.andersen.hw3.ConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class StudentRepository {

    public List<Student> listOfStudents = new ArrayList<>();

    @Autowired
    private ConfigProperties props;

    public void parseFile() throws FileNotFoundException {
        try {
            FileReader file = new FileReader(props.getDataPath());
            listOfStudents = new CsvToBeanBuilder<Student>(file)
                    .withType(Student.class)
                    .build()
                    .parse();
        } catch (FileNotFoundException e) {
            throw e;
        }
    }
}