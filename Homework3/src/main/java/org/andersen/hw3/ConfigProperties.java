package org.andersen.hw3;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("spring.boot.config")
@Getter
@Setter
public class ConfigProperties {
    private String dataPath;
}
