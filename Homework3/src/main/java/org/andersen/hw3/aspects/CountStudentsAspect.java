package org.andersen.hw3.aspects;

import org.andersen.hw3.student.Student;
import org.andersen.hw3.student.StudentRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Aspect
@Component
public class CountStudentsAspect {

    public static HashMap<String, Integer> statistics = new HashMap<>();

    @AfterReturning(pointcut = "execution(* org.andersen.hw3.student.StudentService.getBusyStudents())",
            returning = "students")
    public void CountStudentsAround(JoinPoint joinPoint, List<Student> students) {
        for (var student: students) {
            statistics.merge(student.getName(), 1, (oldValue, newValue) -> oldValue + 1);
        }
    }
}
