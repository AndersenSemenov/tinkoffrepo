# TinkoffRepo

Repository for Tinkoff Fintech hometasks

О себе:

Привет! Меня зовут Андрей Семенов, я студент 2 курса матмеха СПбГУ, программы "Технологии программирования", родился и жил до поступления в Новосибирске, сейчас переехал в Санкт-Петербург. 

Опыт:

В школе интересовался математикой и интересными алгоритмическими задачками, писал на C++. На 1 курсе писал в 1 семестре на F# классические алгоритмы и структуры данных, во 2 семестре - перешел на C#, реализовал симулятор игры "Монополия" с классическом объектно-ориентированной архитектурой классов, познакомился с UML-диаграммами, также реализовал утилиту для работы с файловой системой (на подобии file commander). Летом участвовал в летней школе при СПбГУ, познакомился с азами фулстак-разработки и базами данных, использовал Flask(Python), Bootstrap(HTML), SQLite. В 3 семестре реализовал следующий проект: распарсил и собрал данные, создал базу данных на основе полученных данных и создал небольшой веб-сервис с фильмами, информацией о них(актеры, теги, рейтинг, режиссер) и возможностью поиска по фильмам. Для реализации были использованы Entity FrameWork(C#) и Blazor(C#).

Цели и планы:

Меня интересует бэкенд-разработка, я хочу стать специалистом с глубокими знаниями в этой области, иметь знания и представление о других областях мира программирования. Я интересовался Java как языком считающемся классикой для бэкенд-разработки, на курсе я бы хотел получить максимально практические и применимые знания в области разработки, создать свой веб-сервис на основе полученных знаний. Курс, помимо интересного мне стека технологий и программы, также заинтересовал обучению у людей, работающих в этой области и использующих это каждый день: в университете я получаю часто общие представления о чем-то или фундаментальные знания и основы, курс же привлекает своей практикоориентированностью и возможностью обучения у специалистов в области разработки на Java. 

Я настроен много учиться и разбираться и с нетерпением жду новых лекций и домашних заданий, очень рад, что имею возможность на Финтехе:)
