package org.andersen.hw4.course;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.andersen.hw4.ApplicationIntegrationTest;
import org.andersen.hw4.aspect.CacheAspect;
import org.andersen.hw4.student.Student;
import org.andersen.hw4.user.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "$2a$10$P/.2F8HNssGZMD8nPUchYOvGZhziZgxfUm5kqkY4ZzGX18opM/m7O", roles = {"ADMIN"})
public class CourseControllerTest extends ApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private UserService userService;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
        CacheAspect.cacheStore.clear();
    }

    @Test
    public void create() throws Exception {
        CourseDto courseDto = new CourseDto("new Course", "some text", 4);
        Course expectedCourse = new Course(3L, "new Course", "some text", 4);

        String content = objectMapper.writeValueAsString(courseDto);
        var a = userService.getAll();

        mockMvc.perform(post("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedCourse, courseRepository.findById(3L).get()));
    }

    @Test
    public void createWithValidationError() throws Exception {
        CourseDto courseDto = new CourseDto("", "", 4);

        String content = objectMapper.writeValueAsString(courseDto);

        mockMvc.perform(post("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ValidationException))
                .andExpect(result -> assertEquals("Validation error while trying to create course", result.getResolvedException().getMessage()));
    }

    @Test
    public void addStudentToCourse() throws Exception {
        AddStudentInCourseRequest request = new AddStudentInCourseRequest(2L, 1L);
        Course course = new Course(2L, "Literature", "Pushkin biography", 3);
        Student student = new Student(1L, "Andrey", 10, 1, 2, 5);

        String content = objectMapper.writeValueAsString(request);

        mockMvc.perform(post("/course/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(student, courseRepository.getAllStudentsFromCourse(course).get(1)));
    }

    @Test
    public void addStudentToCourseValidationError() throws Exception {
        AddStudentInCourseRequest request = new AddStudentInCourseRequest(1L, 2L);

        String content = objectMapper.writeValueAsString(request);

        String expectedMessage = "Student with id = 2 hasn't enough grade for course with id = 1";

        mockMvc.perform(post("/course/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ValidationException))
                .andExpect(result -> assertEquals(expectedMessage, result.getResolvedException().getMessage()));
    }

    @Test
    public void addAlreadyStudiedStudentToCourse() throws Exception {
        AddStudentInCourseRequest request = new AddStudentInCourseRequest(1L, 1L);

        String content = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/course/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof DuplicateKeyException));
    }

    @Test
    public void getById() throws Exception {
        Long id = 1L;
        Course course = new Course(id, "Maths", "Euclidean geometry", 4);

        String expectedContent = objectMapper.writeValueAsString(course);

        mockMvc.perform(MockMvcRequestBuilders.get("/course/" + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getByNonExistingId() throws Exception {
        Long id = 4L;

        mockMvc.perform(MockMvcRequestBuilders.get("/course/" + id))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No course was found with id = 4", result.getResolvedException().getMessage()));
    }

    @Test
    public void getAll() throws Exception {
        List<Course> courses = List.of(new Course(1L, "Maths", "Euclidean geometry", 4),
                new Course(2L, "Literature", "Pushkin biography", 3));

        String content = objectMapper.writeValueAsString(courses);

        mockMvc.perform(MockMvcRequestBuilders.get("/course"))
                .andExpect(status().isOk())
                .andExpect(content().string(content));
    }

    @Test
    public void getStudentsFromCourse() throws Exception {
        List<Student> students = List.of(new Student(1L, "Andrey", 10, 1, 2, 5),
                new Student(3L, "Rain", 16, 4, 5, 4));

        String content = objectMapper.writeValueAsString(students);

        mockMvc.perform(MockMvcRequestBuilders.get("/course/1/students"))
                .andExpect(status().isOk())
                .andExpect(content().string(content));
    }

    @Test
    public void getStudentsFromCourseWithNonExistingId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/course/4/students"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No course was found with id = 4", result.getResolvedException().getMessage()));
    }

    @Test
    public void update() throws Exception {
        Course course = new Course(1L, "Maths", "Algebra", 5);

        String content = objectMapper.writeValueAsString(course);

        mockMvc.perform(put("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(course, courseRepository.findById(1L).get()));
    }

    @Test
    public void updateWithValidationError() throws Exception {
        Course course = new Course(1L, "Maths", "Algebra", 77);

        String content = objectMapper.writeValueAsString(course);

        mockMvc.perform(put("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ValidationException))
                .andExpect(result -> assertEquals("Validation error while trying to update course", result.getResolvedException().getMessage()));
    }

    @Test
    public void updateByNonExistingId() throws Exception {
        Course course = new Course(36L, "Maths", "Algebra", 3);

        String content = objectMapper.writeValueAsString(course);

        mockMvc.perform(put("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No course to update with id = 36", result.getResolvedException().getMessage()));
    }

    @Test
    public void deleteById() throws Exception {
        mockMvc.perform(delete("/course/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(x -> assertFalse(courseRepository.findById(1L).isPresent()));
    }

    @Test
    public void deleteByNonExistingId() throws Exception {
        mockMvc.perform(delete("/course/{id}", 3L))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No course to delete with id = 3", result.getResolvedException().getMessage()));
    }

    @Test
    public void deleteAll() throws Exception {
        mockMvc.perform(delete("/course"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }
}
