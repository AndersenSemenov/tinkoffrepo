package org.andersen.hw4.course;

import org.andersen.hw4.ApplicationIntegrationTest;
import org.andersen.hw4.student.Student;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CourseRepositoryTest extends ApplicationIntegrationTest {

    @Autowired
    private CourseRepository courseRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void findWithHighestAvgAge() {
        Course expectedCourse = new Course(2L, "Literature", "Pushkin biography", 3);

        assertEquals(expectedCourse, courseRepository.findWithHighestAvgAge().get());
    }

    @Test
    public void create() {
        courseRepository.create(new Course(null, "new Course", "some text", 4));
        Course expectedCourse = new Course(3L, "new Course", "some text", 4);

        assertEquals(expectedCourse, courseRepository.findById(3L).get());
    }

    @Test
    public void findById() {
        Course expectedCourse = new Course(2L, "Literature", "Pushkin biography", 3);

        assertEquals(expectedCourse, courseRepository.findById(2L).get());
    }

    @Test
    public void findAll() {
        List<Course> courses = List.of(new Course(1L, "Maths", "Euclidean geometry", 4),
                new Course(2L, "Literature", "Pushkin biography", 3));

        assertEquals(courses, courseRepository.findAll());
    }

    @Test
    public void update() {
        Course updatedCourse = new Course(1L, "Maths", "Algebra", 4);

        courseRepository.update(updatedCourse);

        assertEquals(updatedCourse, courseRepository.findById(1L).get());
    }

    @Test
    public void deleteAll() {
        courseRepository.deleteAll();

        assertEquals(0, courseRepository.findAll().size());
    }

    @Test
    public void deleteById() {
        List<Course> expectedCourses = List.of(new Course(2L, "Literature", "Pushkin biography", 3));

        courseRepository.deleteById(1L);

        assertEquals(expectedCourses, courseRepository.findAll());
    }

    @Test
    public void addStudentToCourse() {
        Course course = new Course(2L, "Literature", "Pushkin biography", 3);
        Student student = new Student(1L, "Andrey", 10, 1, 2, 5);

        courseRepository.addStudentToCourse(course, student);

        assertEquals(student, courseRepository.getAllStudentsFromCourse(course).get(1));
    }

    @Test
    public void getAllStudentsFromCourse() {
        Course course = new Course(1L, "Maths", "Euclidean geometry", 4);
        List<Student> students = List.of(new Student(1L, "Andrey", 10, 1, 2, 5),
                new Student(3L, "Rain", 16, 4, 5, 4));

        assertEquals(students, courseRepository.getAllStudentsFromCourse(course));
    }
}
