package org.andersen.hw4.course;

import org.andersen.hw4.student.Student;
import org.andersen.hw4.student.StudentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;

public class CourseServiceTest {

    private CourseRepository courseRepository;
    private StudentService studentService;
    private CourseService courseService;

    @BeforeEach
    public void setUp() {
        courseRepository = Mockito.mock(CourseRepository.class);
        studentService = Mockito.mock(StudentService.class);

        courseService = new CourseService(courseRepository, studentService);
    }

    @Test
    public void createTest() {
        CourseDto courseDto = new CourseDto("new Course", "...", 4);
        Course expectedCourse = new Course(null, "new Course", "...", 4);

        doNothing().when(courseRepository).create(expectedCourse);
        Course createdCourse = courseService.create(courseDto);

        assertEquals(createdCourse, expectedCourse);
    }

    @Test
    public void getWithHighestAvgAgeTest() {
        Course expectedCourse = new Course(2L, "Literature", "Pushkin biography", 3);

        Mockito.when(courseRepository.findWithHighestAvgAge()).thenReturn(Optional.of(expectedCourse));
        Course actualCourse = courseService.getWithHighestAvgAge();

        assertEquals(expectedCourse, actualCourse);
    }

    @Test
    public void getByIdExistingCourseTest() {
        Long id = 1L;
        Course expectedCourse = new Course(id, "Maths", "Euclidean geometry", 4);

        Mockito.when(courseRepository.findById(id)).thenReturn(Optional.of(expectedCourse));
        Course actualCourse = courseService.getById(id);

        assertEquals(actualCourse, expectedCourse);
    }

    @Test
    public void getAll() {
        List<Course> expectedCourses = List.of(new Course(1L, "Maths", "Euclidean geometry", 4),
                new Course(2L, "new Literature", "Lermontov", 3));

        Mockito.when(courseRepository.findAll()).thenReturn(expectedCourses);
        List<Course> actualCourses = courseService.getAll();

        assertEquals(actualCourses, expectedCourses);
    }

    @Test
    public void getAllStudentsFromCourse() {
        Course course = new Course(1L, "Maths", "Euclidean geometry", 4);
        List<Student> expectedStudents = List.of(new Student(1L, "Andrey", 10, 1, 2, 5),
                new Student(2L, "Vasya", 15, 10, 23, 3));

        Mockito.when(courseRepository.getAllStudentsFromCourse(course)).thenReturn(expectedStudents);
        List<Student> actualStudents = courseService.getAllStudentsFromCourse(course);

        assertEquals(actualStudents, expectedStudents);
    }

    @Test
    public void getByIdNonExistingCourseTest() {
        EntityNotFoundException exception = assertThrows(
                EntityNotFoundException.class,
                () -> courseService.getById(5L)
        );

        assertEquals("No course was found with id = 5", exception.getMessage());
    }

    @Test
    public void updateNonExistingCourse() {
        Course updatedCourse = new Course(5L, "Fifth", "new course №5", 5);

        EntityNotFoundException exception = assertThrows(
                EntityNotFoundException.class,
                () -> courseService.update(updatedCourse)
        );

        assertEquals("No course to update with id = 5", exception.getMessage());
    }

    @Test
    public void deleteNonExistingCourse() {
        EntityNotFoundException exception = assertThrows(
                EntityNotFoundException.class,
                () -> courseService.deleteById(6L)
        );

        assertEquals("No course to delete with id = 6", exception.getMessage());
    }

    @Test
    public void addNonExistingStudentToNonExistingCourse() {
        EntityNotFoundException exception = assertThrows(
                EntityNotFoundException.class,
                () -> courseService.addStudentToCourse(4L, 3L)
        );

        assertEquals("No course was found with id = 4", exception.getMessage());
    }
}
