package org.andersen.hw4.course;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.andersen.hw4.ApplicationIntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class CourseControllerSecurityTest extends ApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void getaWithoutAuthentication() throws Exception {
        mockMvc.perform(get("/course"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "user", password = "$2a$12$w8YwXXwR1XFBDl9RaAuvFu2VOBfYoQRiSEq8CZqoXDK8Uxg5aDjuy")
    public void createWithoutAdminRights() throws Exception {
        CourseDto courseDto = new CourseDto("new Course", "some text", 4);

        String content = objectMapper.writeValueAsString(courseDto);
        mockMvc.perform(post("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "user", password = "$2a$12$w8YwXXwR1XFBDl9RaAuvFu2VOBfYoQRiSEq8CZqoXDK8Uxg5aDjuy")
    public void updateWithoutAdminRights() throws Exception {
        Course course = new Course(1L, "Maths", "Algebra", 5);

        String content = objectMapper.writeValueAsString(course);
        mockMvc.perform(put("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "user", password = "$2a$12$w8YwXXwR1XFBDl9RaAuvFu2VOBfYoQRiSEq8CZqoXDK8Uxg5aDjuy")
    public void deleteWithoutAdminRights() throws Exception {
        mockMvc.perform(delete("/course"))
                .andExpect(status().isForbidden());
    }
}
