package org.andersen.hw4.course;

import org.andersen.hw4.ApplicationIntegrationTest;
import org.andersen.hw4.aspect.CacheAspect;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CourseCacheServiceTest extends ApplicationIntegrationTest {

    @Autowired
    private CourseService courseService;

    @SpyBean
    private CourseRepository courseRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
        CacheAspect.cacheStore.clear();
    }

    @Test
    public void getCourseWithNewCache() {
        courseService.getById(1L);

        verify(courseRepository, times(1))
                .findById(any());

        assertEquals(Map.of(1L, new Course(1L, "Maths", "Euclidean geometry", 4)),
                CacheAspect.cacheStore);
    }

    @Test
    public void getCourseWithExistedCache() {
        Long id = 2L;
        CacheAspect.cacheStore.put(id, new Course(id, "Literature", "Pushkin biography", 3));

        courseService.getById(id);

        verify(courseRepository, times(0))
                .findById(any());
    }

    @Test
    public void createCourseCache() {
        courseService.create(new CourseDto("o", "[", 4));

        assertEquals(Map.of(3L, new Course(3L, "o", "[", 4)),
                CacheAspect.cacheStore);
    }

    @Test
    public void updateCourseCache() {
        courseService.update(new Course(2L, "o", "P", 3));

        assertEquals(Map.of(2L, new Course(2L, "o", "P", 3)),
                CacheAspect.cacheStore);
    }

    @Test
    public void deleteCourseCache() {
        courseService.getById(2L);

        courseService.deleteById(2L);

        assertEquals(0, CacheAspect.cacheStore.size());
    }
}
