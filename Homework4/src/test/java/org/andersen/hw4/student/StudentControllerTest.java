package org.andersen.hw4.student;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.andersen.hw4.ApplicationIntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "$2a$10$P/.2F8HNssGZMD8nPUchYOvGZhziZgxfUm5kqkY4ZzGX18opM/m7O", roles = {"ADMIN"})
public class StudentControllerTest extends ApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private StudentRepository studentRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void create() throws Exception {
        StudentDto studentDto = new StudentDto("Tay", 4, 12, 16, 4);
        Student expectedStudent = new Student(4L, "Tay", 4, 12, 16, 4);

        String content = objectMapper.writeValueAsString(studentDto);
        mockMvc.perform(post("/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedStudent, studentRepository.findById(4L).get()));
    }

    @Test
    public void createWithValidationError() throws Exception {
        StudentDto studentDto = new StudentDto("Tay", 4, 12, 16, 6);

        String content = objectMapper.writeValueAsString(studentDto);

        mockMvc.perform(post("/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ValidationException))
                .andExpect(result -> assertEquals("Validation error while trying to create student", result.getResolvedException().getMessage()));
    }

    @Test
    public void getById() throws Exception {
        Long id = 1L;

        Student student = new Student(1L, "Andrey", 10, 1, 2, 5);

        String expectedContent = objectMapper.writeValueAsString(student);

        mockMvc.perform(get("/student/" + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void getByNonExistingId() throws Exception {
        Long id = 4L;

        mockMvc.perform(MockMvcRequestBuilders.get("/student/" + id))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No student was found with id = 4", result.getResolvedException().getMessage()));
    }

    @Test
    public void getAll() throws Exception {
        List<Student> students = List.of(new Student(1L, "Andrey", 10, 1, 2, 5),
                new Student(2L, "Vasya", 15, 10, 23, 3),
                new Student(3L, "Rain", 16, 4, 5, 4));

        String expectedContent = objectMapper.writeValueAsString(students);

        mockMvc.perform(get("/student"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    public void update() throws Exception {
        Student updatedStudent = new Student(1L, "Poqw", 3, 4, 5, 5);

        String expectedContent = objectMapper.writeValueAsString(updatedStudent);

        mockMvc.perform(put("/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(updatedStudent, studentRepository.findById(1L).get()));
    }

    @Test
    public void updateWithValidationError() throws Exception {
        Student updatedStudent = new Student(1L, "Poqw", -1, 4, 5, 5);

        String expectedContent = objectMapper.writeValueAsString(updatedStudent);

        mockMvc.perform(put("/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ValidationException))
                .andExpect(result -> assertEquals("Validation error while trying to update student", result.getResolvedException().getMessage()));
    }

    @Test
    public void deleteById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/student/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(x -> assertFalse(studentRepository.findById(1L).isPresent()));
    }

    @Test
    public void deleteByNonExistingId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/student/{id}", 60L))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No student to delete with id = 60", result.getResolvedException().getMessage()));
        ;
    }

    @Test
    public void delete() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/student"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }
}
