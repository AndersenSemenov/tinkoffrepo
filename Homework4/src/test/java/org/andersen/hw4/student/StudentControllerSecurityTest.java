package org.andersen.hw4.student;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.andersen.hw4.ApplicationIntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
public class StudentControllerSecurityTest extends ApplicationIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void getWithoutAuthentication() throws Exception {
        mockMvc.perform(get("/student"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "user", password = "$2a$12$w8YwXXwR1XFBDl9RaAuvFu2VOBfYoQRiSEq8CZqoXDK8Uxg5aDjuy")
    public void deleteWithoutAdminRights() throws Exception {
        mockMvc.perform(delete("/student"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "user", password = "$2a$12$w8YwXXwR1XFBDl9RaAuvFu2VOBfYoQRiSEq8CZqoXDK8Uxg5aDjuy")
    public void updateWithoutAdminRights() throws Exception {
        Student updatedStudent = new Student(1L, "Poqw", 3, 4, 5, 5);

        String expectedContent = objectMapper.writeValueAsString(updatedStudent);
        mockMvc.perform(put("/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "user", password = "$2a$12$w8YwXXwR1XFBDl9RaAuvFu2VOBfYoQRiSEq8CZqoXDK8Uxg5aDjuy")
    public void createWithoutAdminRights() throws Exception {
        StudentDto studentDto = new StudentDto("Tay", 4, 12, 16, 4);

        String content = objectMapper.writeValueAsString(studentDto);
        mockMvc.perform(post("/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isForbidden());
    }
}
