package org.andersen.hw4.student;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;

class StudentServiceTest {

    private StudentService studentService;
    private StudentRepository studentRepository;

    @BeforeEach
    public void setUp() {
        studentRepository = Mockito.mock(StudentRepository.class);
        studentService = new StudentService(studentRepository);
    }

    @Test
    public void create() {
        StudentDto studentDto = new StudentDto("Hywqe", 12, 12, 16, 5);
        Student expectedStudent = new Student(null, "Hywqe", 12, 12, 16, 5);

        doNothing().when(studentRepository).create(expectedStudent);
        Student actualStudent = studentService.create(studentDto);

        assertEquals(expectedStudent, actualStudent);
    }

    @Test
    public void getById() {
        Long id = 2L;
        Student expectedStudent = new Student(id, "Vasya", 15, 10, 23, 3);

        Mockito.when(studentRepository.findById(id)).thenReturn(Optional.of(expectedStudent));
        Student actualStudent = studentService.getById(id);

        assertEquals(expectedStudent, actualStudent);
    }

    @Test
    public void getAll() {
        List<Student> expectedStudents = List.of(new Student(1L, "newAndrey", 10, 1, 2, 5),
                new Student(2L, "Vasya", 15, 10, 23, 3),
                new Student(3L, "Rain", 16, 4, 5, 4));

        Mockito.when(studentRepository.findAll()).thenReturn(expectedStudents);
        List<Student> actualStudents = studentService.getAll();

        assertEquals(actualStudents, expectedStudents);
    }

    @Test
    public void getByNonExistingId() {
        EntityNotFoundException exception = assertThrows(
                EntityNotFoundException.class,
                () -> studentService.getById(4L)
        );

        Assertions.assertEquals("No student was found with id = 4", exception.getMessage());
    }

    @Test
    public void updateByNonExistingId() {
        EntityNotFoundException exception = assertThrows(
                EntityNotFoundException.class,
                () -> studentService.update(new Student(4L, "a", 12, 2, 3, 4))
        );

        Assertions.assertEquals("No student to update with id = 4", exception.getMessage());
    }

    @Test
    public void deleteByNonExistingId() {
        EntityNotFoundException exception = assertThrows(
                EntityNotFoundException.class,
                () -> studentService.deleteById(4L)
        );

        Assertions.assertEquals("No student to delete with id = 4", exception.getMessage());
    }
}