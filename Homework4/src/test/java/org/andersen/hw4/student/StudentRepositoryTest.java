package org.andersen.hw4.student;

import org.andersen.hw4.ApplicationIntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StudentRepositoryTest extends ApplicationIntegrationTest {

    @Autowired
    private StudentRepository studentRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void create() {
        studentRepository.create(new Student(null, "Tay", 4, 12, 16, 4));
        Student expectedStudent = new Student(4L, "Tay", 4, 12, 16, 4);

        assertEquals(expectedStudent, studentRepository.findById(4L).get());
    }

    @Test
    public void getById() {
        Student expectedStudent = new Student(1L, "Andrey", 10, 1, 2, 5);

        assertEquals(expectedStudent, studentRepository.findById(1L).get());
    }

    @Test
    public void getAll() {
        List<Student> expectedStudents = List.of(new Student(1L, "Andrey", 10, 1, 2, 5),
                new Student(2L, "Vasya", 15, 10, 23, 3),
                new Student(3L, "Rain", 16, 4, 5, 4));

        assertEquals(expectedStudents, studentRepository.findAll());
    }

    @Test
    public void update() {
        Student updatedStudent = new Student(1L, "newAndrey", 10, 1, 2, 5);

        studentRepository.update(updatedStudent);

        assertEquals(updatedStudent, studentRepository.findById(1L).get());
    }


    @Test
    public void deleteById() {
        studentRepository.deleteById(1L);

        assertFalse(studentRepository.findById(1L).isPresent());
    }

    @Test
    public void deleteAll() {
        studentRepository.deleteAll();

        assertEquals(0, studentRepository.findAll().size());
    }
}
