package org.andersen.hw4.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.andersen.hw4.ApplicationIntegrationTest;
import org.andersen.hw4.role.Role;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "$2a$10$P/.2F8HNssGZMD8nPUchYOvGZhziZgxfUm5kqkY4ZzGX18opM/m7O", roles = {"ADMIN"})
public class UserControllerTest extends ApplicationIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void getAll() throws Exception {
        List<User> expectedUsers = List.of(new User(1L, "admin", "$2a$10$P/.2F8HNssGZMD8nPUchYOvGZhziZgxfUm5kqkY4ZzGX18opM/m7O", new Role(2L,"ROLE_ADMIN")),
                new User(2L, "user", "$2a$12$w8YwXXwR1XFBDl9RaAuvFu2VOBfYoQRiSEq8CZqoXDK8Uxg5aDjuy", new Role(1L,"ROLE_USER")));

        String content = objectMapper.writeValueAsString(expectedUsers);

        mockMvc.perform(get("/user"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(content));
    }

    @Test
    public void save() throws Exception {
        UserDto userDto = new UserDto("third", "somePass");

        String content = objectMapper.writeValueAsString(userDto);
        mockMvc.perform(post("/user")
                        .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isOk());

        assertTrue(userRepository.findByUserName("third") != null);
    }
}
