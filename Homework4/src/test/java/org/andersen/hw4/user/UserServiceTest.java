package org.andersen.hw4.user;

import org.andersen.hw4.role.Role;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserServiceTest {

    private UserRepository userRepository;
    private UserService userService;
    private BCryptPasswordEncoder encoder;

    @BeforeEach
    void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        userService = new UserService(userRepository);
        encoder = new BCryptPasswordEncoder();
    }

    @Test
    public void loadUserByName() {
        User adminUser = new User(1L, "admin", "$2a$10$P/.2F8HNssGZMD8nPUchYOvGZhziZgxfUm5kqkY4ZzGX18opM/m7O", new Role(2L,"ROLE_ADMIN"));
        Mockito.when(userRepository.findByUserName("admin")).thenReturn(adminUser);
        UserPrincipal loadedUser = (UserPrincipal)userService.loadUserByUsername("admin");
        assertEquals(adminUser, loadedUser.getUser());
    }

    @Test
    public void getAll() {
        List<User> expectedUsers = List.of(new User(1L, "first", "soqwe", new Role(1L, "ROLE_USER")));

        Mockito.when(userRepository.findAll()).thenReturn(expectedUsers);
        List<User> users = userService.getAll();

        assertEquals(expectedUsers, users);
    }
}
