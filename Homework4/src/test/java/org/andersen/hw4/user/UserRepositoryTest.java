package org.andersen.hw4.user;

import org.andersen.hw4.ApplicationIntegrationTest;
import org.andersen.hw4.role.Role;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserRepositoryTest extends ApplicationIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void findByUserName() {
        User user = userRepository.findByUserName("user");
        User expectedUser = new User(2L, "user", "$2a$12$w8YwXXwR1XFBDl9RaAuvFu2VOBfYoQRiSEq8CZqoXDK8Uxg5aDjuy", new Role(1L,"ROLE_USER"));
        assertEquals(expectedUser, user);
    }

    @Test
    public void findAll() {
        List<User> users = userRepository.findAll();
        List<User> expectedUsers = List.of(new User(1L, "admin", "$2a$10$P/.2F8HNssGZMD8nPUchYOvGZhziZgxfUm5kqkY4ZzGX18opM/m7O", new Role(2L,"ROLE_ADMIN")),
                new User(2L, "user", "$2a$12$w8YwXXwR1XFBDl9RaAuvFu2VOBfYoQRiSEq8CZqoXDK8Uxg5aDjuy", new Role(1L,"ROLE_USER")));
        assertEquals(expectedUsers, users);
    }

    @Test
    public void save() {
        User user = new User(null, "thirdUser", "some password", null);
        User savedUser = new User(3L, "thirdUser", "some password", new Role(1L, "ROLE_USER"));
        userRepository.saveUser(user);
        assertTrue(userRepository.findAll().contains(savedUser));
    }
}
