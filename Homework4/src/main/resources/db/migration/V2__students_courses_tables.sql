create table if not exists students(
    id        bigserial primary key,
    name      varchar(50) not null,
    age		  integer not null,
    time_from integer not null,
    time_to   integer not null,
    grade     integer not null,
    fk_user_id bigint unique references users(id)
);

create table if not exists courses(
    id			   bigserial primary key,
    name		   varchar(50) not null,
    description    varchar(1024),
    required_grade integer not null
);

create table if not exists course_student(
	course_id  bigint,
	student_id bigint,
	foreign key(course_id)  references courses(id) on update cascade on delete cascade,
    foreign key(student_id) references students(id) on update cascade on delete cascade,
    primary key(course_id, student_id)
);

create index  if not exists index_course_group on courses(id, name, description);
create index if not exists fk_index_course_id on course_student(course_id);
create index if not exists fk_index_student_id on course_student(student_id);

insert into students(name, age, time_from, time_to, grade) values('Andrey', 10, 1, 2, 5);
insert into students(name, age, time_from, time_to, grade) values('Vasya', 15, 10, 23, 3);
insert into students(name, age, time_from, time_to, grade) values('Rain', 16, 4, 5, 4);

insert into courses(name, description, required_grade) values('Maths', 'Euclidean geometry', 4);
insert into courses(name, description, required_grade) values('Literature', 'Pushkin biography', 3);

insert into course_student(course_id, student_id) values(1, 1);
insert into course_student(course_id, student_id) values(1, 3);
insert into course_student(course_id, student_id) values(2, 2);
insert into course_student(course_id, student_id) values(2, 3);