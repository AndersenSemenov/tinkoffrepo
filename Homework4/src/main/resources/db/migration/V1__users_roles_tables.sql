create table if not exists roles(
	id   bigserial primary key,
	name varchar(255) not null
);

create table if not exists users(
	id            bigserial primary key,
	name          varchar(255) not null,
	password      varchar(255) not null,
	fk_role_id    bigint references roles(id)
);

insert into roles(name) values('ROLE_USER');
insert into roles(name) values('ROLE_ADMIN');

insert into users(name, password, fk_role_id) values('admin', '$2a$10$P/.2F8HNssGZMD8nPUchYOvGZhziZgxfUm5kqkY4ZzGX18opM/m7O', 2);
insert into users(name, password, fk_role_id) values('user', '$2a$12$w8YwXXwR1XFBDl9RaAuvFu2VOBfYoQRiSEq8CZqoXDK8Uxg5aDjuy', 1);