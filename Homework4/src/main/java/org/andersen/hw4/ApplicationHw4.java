package org.andersen.hw4;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
@ImportAutoConfiguration
public class ApplicationHw4 {

    @Autowired
    private Flyway flyway;

    @Component
    public class FlywayMigrationRefresh implements ApplicationRunner {
        @Override
        public void run(ApplicationArguments args) throws Exception {
            flyway.clean();
            flyway.migrate();
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(ApplicationHw4.class, args);
    }
}
