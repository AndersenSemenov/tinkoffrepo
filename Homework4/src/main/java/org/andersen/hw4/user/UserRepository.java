package org.andersen.hw4.user;

import org.andersen.hw4.role.Role;
import org.andersen.hw4.role.RoleRepository;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserRepository extends RoleRepository {

    @Select("""
            select id,
                   name,
                   password,
                   fk_role_id
              from users
             where name=#{name}
            """)
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "password", column = "password"),
            @Result(property = "role", column = "fk_role_id",
                    javaType = Role.class, one = @One(select="getRoleById"))
    })
    User findByUserName(@Param("name") String name);

    @Select("""
            select id,
                   name,
                   password,
                   fk_role_id
              from users
            """)
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "password", column = "password"),
            @Result(property = "role", column = "fk_role_id",
                    javaType = Role.class, one = @One(select="getRoleById"))
    })
    List<User> findAll();

    @Insert("""
            insert into users(name, password, fk_role_id)
            values (#{user.name}, #{user.password}, 1);
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void saveUser(@Param("user") User user);
}
