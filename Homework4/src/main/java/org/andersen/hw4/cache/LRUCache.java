package org.andersen.hw4.cache;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.andersen.hw4.course.CourseConstant.CACHE_SIZE;

public class LRUCache<K, V> extends LinkedHashMap<K, V> {

    private int capacity;

    public LRUCache() {
        super(CACHE_SIZE, 0.75f, true);
        capacity = CACHE_SIZE;
    }

    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > capacity;
    }
}
