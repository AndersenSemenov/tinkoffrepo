package org.andersen.hw4.role;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoleService {

    private RoleRepository roleRepository;

    public Role getRoleById(Long id) {
        return roleRepository.getRoleById(id);
    }
}
