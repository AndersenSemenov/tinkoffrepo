package org.andersen.hw4.role;

import org.apache.ibatis.annotations.*;

@Mapper
public interface RoleRepository {

    @Select("""
            select id,
                   name
              from roles
             where id=#{id}
            """)
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    Role getRoleById(@Param("id") Long id);
}
