package org.andersen.hw4.student;

import lombok.AllArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.List;

@RestController
@RequestMapping("/student")
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public Student create(@Valid @RequestBody StudentDto studentDto, BindingResult errors) {
        if (errors.hasErrors()) {
            throw new ValidationException("Validation error while trying to create student");
        }
        return studentService.create(studentDto);
    }

    @GetMapping("/{id}")
    public Student getById(@PathVariable("id") Long id) {
        return studentService.getById(id);
    }

    @GetMapping
    public List<Student> getAll() {
        return studentService.getAll();
    }

    @PutMapping
    public void update(@Valid @RequestBody Student student, BindingResult errors) {
        if (errors.hasErrors()) {
            throw new ValidationException("Validation error while trying to update student");
        }
        studentService.update(student);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        studentService.deleteById(id);
    }

    @DeleteMapping
    public void deleteAll() {
        studentService.deleteAll();
    }
}
