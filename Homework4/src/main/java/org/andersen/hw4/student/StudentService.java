package org.andersen.hw4.student;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@AllArgsConstructor
public class StudentService {

    private final StudentRepository repository;

    public Student create(StudentDto studentDto) {
        Student student = new Student();

        student.setName(studentDto.getName());
        student.setAge(studentDto.getAge());
        student.setTimeFrom(studentDto.getTimeFrom());
        student.setTimeTo(studentDto.getTimeTo());
        student.setGrade(studentDto.getGrade());

        repository.create(student);
        return student;
    }

    public List<Student> getAll() {
        return repository.findAll();
    }

    public Student getById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No student was found with id = " + id));
    }

    public void update(Student student) {
        if (repository.update(student) == 0) {
            throw new EntityNotFoundException("No student to update with id = " + student.getId());
        }
    }

    public void deleteById(Long id) {
        if (repository.deleteById(id) == 0) {
            throw new EntityNotFoundException("No student to delete with id = " + id);
        }
    }

    public void deleteAll() {
        repository.deleteAll();
    }
}