package org.andersen.hw4.student;

public class StudentValidatorConstant {

    public static final int MIN_AGE = 0;
    public static final int MIN_HOUR = 0;
    public static final int MAX_HOUR = 23;
    public static final int MIN_GRADE = 2;
    public static final int MAX_GRADE = 5;
}
