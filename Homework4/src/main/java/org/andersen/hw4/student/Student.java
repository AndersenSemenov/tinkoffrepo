package org.andersen.hw4.student;

import lombok.*;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import static org.andersen.hw4.student.StudentValidatorConstant.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private Long id;

    @NotBlank(message = "Student name can't be empty")
    private String name;

    @Min(value = MIN_AGE, message = "Age can't be non positive")
    private int age;

    @Range(min = MIN_HOUR, max = MAX_HOUR, message = "Hour has to be from 0 to 23")
    private int timeFrom;

    @Range(min = MIN_HOUR, max = MAX_HOUR, message = "Hour has to be from 0 to 23")
    private int timeTo;

    @Range(min = MIN_GRADE, max = MAX_GRADE, message = "Grade has to be from 2 to 5")
    private int grade;
}
