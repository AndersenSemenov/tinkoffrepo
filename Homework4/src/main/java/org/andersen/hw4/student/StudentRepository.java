package org.andersen.hw4.student;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface StudentRepository {

    @Insert("""
            insert into students(name, age, time_from, time_to, grade)
            values (#{name}, #{age}, #{timeFrom}, #{timeTo}, #{grade})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void create(Student student);

    @Select("""
            select id,
                   name,
                   age,
                   time_from,
                   time_to,
                   grade
              from students
            """)
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "age", property = "age"),
            @Result(column = "time_from", property = "timeFrom"),
            @Result(column = "time_to", property = "timeTo"),
            @Result(column = "grade", property = "grade")})
    List<Student> findAll();

    @Select("""
            select id,
                   name,
                   age,
                   time_from,
                   time_to,
                   grade
              from students
             where id = #{id}
            """)
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "age", property = "age"),
            @Result(column = "time_from", property = "timeFrom"),
            @Result(column = "time_to", property = "timeTo"),
            @Result(column = "grade", property = "grade")})
    Optional<Student> findById(@Param("id") Long id);

    @Update("""
            update students set
              name = #{student.name},
               age = #{student.age},
         time_from = #{student.timeFrom},
           time_to = #{student.timeTo},
             grade = #{student.grade}
             where id = #{student.id}
            """)
    int update(@Param("student") Student student);

    @Delete("""
            delete
              from students
             where id = #{id}
            """)
    int deleteById(@Param("id") Long id);

    @Delete("""
            delete
              from students
            """)
    void deleteAll();
}