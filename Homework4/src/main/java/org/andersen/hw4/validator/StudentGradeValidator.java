package org.andersen.hw4.validator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.andersen.hw4.course.AddStudentInCourseRequest;
import org.andersen.hw4.course.Course;
import org.andersen.hw4.course.CourseService;
import org.andersen.hw4.student.Student;
import org.andersen.hw4.student.StudentService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Getter
@Setter
@AllArgsConstructor
public class StudentGradeValidator implements ConstraintValidator<StudentGradeConstraint, AddStudentInCourseRequest> {

    private final StudentService studentService;
    private final CourseService courseService;

    @Override
    public void initialize(StudentGradeConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(AddStudentInCourseRequest addStudentInCourseRequest, ConstraintValidatorContext constraintValidatorContext) {
        Course course = courseService.getById(addStudentInCourseRequest.getCourseId());
        Student student = studentService.getById(addStudentInCourseRequest.getStudentId());
        return student.getGrade() >= course.getRequiredGrade();
    }
}
