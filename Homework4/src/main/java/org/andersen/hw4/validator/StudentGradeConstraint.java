package org.andersen.hw4.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = StudentGradeValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface StudentGradeConstraint {
    String message() default
            "Student can't study on this course, grade is too small";
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
