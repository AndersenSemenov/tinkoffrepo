package org.andersen.hw4.aspect;

import org.andersen.hw4.cache.LRUCache;
import org.andersen.hw4.course.Course;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;

@Aspect
@Component
public class CacheAspect {

    public static Map<Long, Course> cacheStore = Collections.synchronizedMap(new LRUCache<Long, Course>());

    @AfterReturning(pointcut = "execution(* org.andersen.hw4.course.CourseService.create(..))",
        returning = "course")
    public void cacheCreate(JoinPoint joinPoint, Course course) {
        cacheStore.put(course.getId(), course);
    }

    @After("execution(* org.andersen.hw4.course.CourseService.update(..)) && args(course)")
    public void cacheUpdate(JoinPoint joinPoint, Course course) {
        cacheStore.put(course.getId(), course);
    }

    @Around("execution(* org.andersen.hw4.course.CourseService.getById(..)) && args(id)")
    public Course cacheGet(ProceedingJoinPoint joinPoint, Long id) throws Throwable {
        if (cacheStore.containsKey(id)) {
            return cacheStore.get(id);
        } else {
            Course course = (Course)joinPoint.proceed();
            cacheStore.put(course.getId(), course);
            return course;
        }
    }

    @Before("execution(* org.andersen.hw4.course.CourseService.deleteById(..)) && args(id)")
    public void cacheDelete(JoinPoint joinPoint, Long id) {
        if (cacheStore.containsKey(id)) {
            cacheStore.remove(id);
        }
    }
}
