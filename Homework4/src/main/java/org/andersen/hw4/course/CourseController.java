package org.andersen.hw4.course;

import lombok.AllArgsConstructor;
import org.andersen.hw4.student.Student;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.List;

@RestController
@RequestMapping("/course")
@AllArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @PostMapping
    public Course create(@Valid @RequestBody CourseDto courseDto, BindingResult errors) {
        if (errors.hasErrors()) {
            throw new ValidationException("Validation error while trying to create course");
        }
        return courseService.create(courseDto);
    }

    @PostMapping("/student")
    public void addStudentToCourse(@Valid @RequestBody AddStudentInCourseRequest addStudentInCourseRequest,
                                   BindingResult errors) {
        if (errors.hasErrors()) {
            throw new ValidationException(
                    "Student with id = " + addStudentInCourseRequest.getStudentId() + " hasn't enough grade " +
                            "for course with id = " + addStudentInCourseRequest.getCourseId());
        }

        courseService.addStudentToCourse(addStudentInCourseRequest.getCourseId(),
                addStudentInCourseRequest.getStudentId());
    }

    @GetMapping("/{id}")
    public Course getById(@PathVariable("id") Long id) {
        return courseService.getById(id);
    }

    @GetMapping
    public List<Course> getAll() {
        return courseService.getAll();
    }

    @GetMapping("/{courseId}/students")
    public List<Student> getStudentsByCourse(@PathVariable("courseId") Long courseId) {
        return courseService.getAllStudentsFromCourse(courseService.getById(courseId));
    }

    @PutMapping
    public void update(@Valid @RequestBody Course course, BindingResult errors) {
        if (errors.hasErrors()) {
            throw new ValidationException("Validation error while trying to update course");
        }
        courseService.update(course);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        courseService.deleteById(id);
    }

    @DeleteMapping
    public void deleteAll() {
        courseService.deleteAll();
    }
}
