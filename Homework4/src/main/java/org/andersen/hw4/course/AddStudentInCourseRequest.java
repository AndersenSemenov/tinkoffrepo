package org.andersen.hw4.course;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.andersen.hw4.validator.StudentGradeConstraint;

@Getter
@Setter
@AllArgsConstructor
@StudentGradeConstraint
public class AddStudentInCourseRequest {
    private Long courseId;
    private Long studentId;
}
