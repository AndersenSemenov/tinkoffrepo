package org.andersen.hw4.course;

import org.andersen.hw4.student.Student;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface CourseRepository {

    @Select("""
            select
                   courses.id,
                   courses.name,
                   courses.description,
                   courses.required_grade,
                   avg(age) as average
              from courses
              join course_student on courses.id = course_student.course_id
              join students on course_student.student_id = students.id
             group by courses.id, courses.name, courses.description, courses.required_grade
             order by average desc
             limit 1;
            """)
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "required_grade", property = "requiredGrade")})
    Optional<Course> findWithHighestAvgAge();


    @Insert("""
            insert into courses(name, description, required_grade)
            values (#{name}, #{description}, #{requiredGrade})
            """)
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void create(Course course);

    @Select("""
            select id,
                   name,
                   description,
                   required_grade
              from courses
            """)
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "required_grade", property = "requiredGrade")})
    List<Course> findAll();

    @Select("""
            select id,
                   name,
                   description,
                   required_grade
              from courses
             where id = #{id}
            """)
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "required_grade", property = "requiredGrade")})
    Optional<Course> findById(@Param("id") Long id);

    @Update("""
            update courses set
              name = #{course.name},
       description = #{course.description},
    required_grade = #{course.requiredGrade}
             where id = #{course.id}
            """)
    int update(@Param("course") Course course);

    @Delete("""
            delete
              from courses
            """)
    void deleteAll();

    @Delete("""
            delete
              from courses
             where id = #{id}
            """)
    int deleteById(@Param("id") Long id);

    @Insert("""
            insert into course_student(course_id, student_id)
            values (#{course.id}, #{student.id})
            """)
    int addStudentToCourse(@Param("course") Course course, @Param("student") Student student);

    @Select("""
            select students.id,
                   students.name,
                   students.age,
                   students.time_from,
                   students.time_to,
                   students.grade
              from courses
              join course_student on course_student.course_id = #{course.id}
              join students on course_student.student_id = students.id
             group by students.id, students.name, students.age, students.time_from, students.time_to, students.grade
            """)
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "age", property = "age"),
            @Result(column = "time_from", property = "timeFrom"),
            @Result(column = "time_to", property = "timeTo"),
            @Result(column = "grade", property = "grade")})
    List<Student> getAllStudentsFromCourse(@Param("course") Course course);
}
