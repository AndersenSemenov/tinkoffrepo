package org.andersen.hw4.course;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;

import static org.andersen.hw4.course.CourseConstant.*;


@Data
@Accessors(chain = true)
@AllArgsConstructor
public class CourseDto {
    @NotBlank(message = "Course name can't be empty")
    private String name;

    @NotBlank(message = "Course description can't be empty")
    private String description;

    @Range(min = MIN_GRADE, max = MAX_GRADE, message = "Grade has to be from 2 to 5")
    private int requiredGrade;
}
