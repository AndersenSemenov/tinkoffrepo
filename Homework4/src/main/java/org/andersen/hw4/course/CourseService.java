package org.andersen.hw4.course;

import lombok.AllArgsConstructor;
import org.andersen.hw4.annotation.CacheCheck;
import org.andersen.hw4.student.Student;
import org.andersen.hw4.student.StudentService;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class CourseService {
    private final CourseRepository courseRepository;
    private final StudentService studentService;

    public Course getWithHighestAvgAge() {
        return courseRepository.findWithHighestAvgAge()
                .orElseThrow(() -> new EntityNotFoundException("No course with highest average age was found"));
    }

    @CacheCheck
    public Course create(CourseDto courseDto) {
        Course course = new Course();
                course.setName(courseDto.getName());
                course.setDescription(courseDto.getDescription());
                course.setRequiredGrade(courseDto.getRequiredGrade());

        courseRepository.create(course);
        return course;
    }

    public List<Course> getAll() {
        return courseRepository.findAll();
    }

    @CacheCheck
    public Course getById(Long id) {
        int a = 5;
        return courseRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No course was found with id = " + id));
    }

    @CacheCheck
    public void update(Course course) {
        if (courseRepository.update(course) == 0) {
            throw new EntityNotFoundException("No course to update with id = " + course.getId());
        }
    }

    public void deleteAll() {
        courseRepository.deleteAll();;
    }

    @CacheCheck
    public void deleteById(Long id) {
        if (courseRepository.deleteById(id) == 0) {
            throw new EntityNotFoundException("No course to delete with id = " + id);
        }
    }

    @Transactional
    public void addStudentToCourse(Long courseId, Long studentId) {
        Course course = this.getById(courseId);
        Student student = studentService.getById(studentId);
        if (courseRepository.addStudentToCourse(course, student) == 0) {
            throw new DuplicateKeyException("Student with id = " + studentId +
                    " already studies on course with id = " + courseId);
        }
    }

    public List<Student> getAllStudentsFromCourse(Course course) {
        return courseRepository.getAllStudentsFromCourse(course);
    }
}
