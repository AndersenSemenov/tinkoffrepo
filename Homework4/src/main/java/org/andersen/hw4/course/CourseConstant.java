package org.andersen.hw4.course;

public class CourseConstant {

    public static final int MIN_GRADE = 2;
    public static final int MAX_GRADE = 5;
    public static final int CACHE_SIZE = 5;
}
